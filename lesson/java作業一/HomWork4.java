
public class HomWork4 {

	public static void main(String[] args) {
		// 請定義一個常數為3.1415(圓周率)，並計算半徑為5的圓面積與圓周長

		double pi = 3.1415;// 因圓周率3.1415有小數點故使用浮點數(double)先宣告其值
		int radius = 5;// 因5為整數故使用(int)宣告
		double circleArea = (radius * radius * pi);// 半徑(5)*半徑(5)*pi(3.1415)=圓面積(circleArea)
		double circumference = (2 * radius * pi);// 2*半徑(5)*pi(3.1415)=圓周長(circumference)
		System.out.println(circleArea);// 故印出圓面積為:78.53750000000001
		System.out.println(circumference);// 故印出圓周長為:31.415000000000003
	}

}

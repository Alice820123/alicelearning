
public class HomeWork5 {

	public static void main(String[] args) {
		// 某人在銀行存入150萬，銀行利率為2%，如果每年利息都繼續存入銀行，請用程式計算10年後，本金加利息共有多少錢(用複利計算，公式請自行google)

		double funds = 1_500_000; // 宣告本金為150萬

		double rate = 0.02;// 計算利息 2%=2/100結果為0.02,150萬+利息=利率

		for (int i = 1; i < 11; i++) {

			double year = (funds * rate);// 第一年本金*利率=第一年利息
			funds = year + funds;// 利息+本金=總金額

			System.out.println(funds);
			System.out.printf("第%d年本金利息有%f\n", i, funds);// 故印出每一年總金額
		}

	}

}

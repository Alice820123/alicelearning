
public class HomeWork3 {
	// 請由程式算出256559秒為多少天、多少小時、多少分與多少秒

	public static void main(String[] args) {

		int totSec = 256559;// 總秒數

		int minutes = 60;// 一分鐘60秒

		int hour = 60 * minutes;// 一小時=60分鐘

		int day = 24 * hour;// 一天 = 24小時

		int totday = (totSec / day); // 總天數 = 總秒數/一天的總秒數

		int leftSecExDay = totSec % day;
		// 計算天的餘秒數= 2數56559(總秒數) - (2*24*60*60) (2天總秒)

		int leftSecExDayAndHour = leftSecExDay % hour;
		// 計算小時的餘秒數=83759(天的餘秒數)-23*60*60(總時數)

		int leftSecExDayAndHourAndMinutes = leftSecExDayAndHour % minutes;
		// 計算小時的餘秒數=959(時的餘秒數)-15*60(總分數)

		System.out.println("256559秒有" + totSec / day + "天");

		System.out.println("256559秒有" + leftSecExDay / hour + "時");

		System.out.println("256559秒有" + leftSecExDayAndHour / minutes + "分");

		System.out.println("256559秒有" + leftSecExDayAndHour % minutes + "秒");

		System.out.printf("256559秒有  %d天 %d時 %d分 %d秒\n", totSec / day, leftSecExDay / hour,
				leftSecExDayAndHour / minutes, leftSecExDayAndHour % minutes);

		// 256559秒有2天
		// 256559秒有23時
		// 256559秒有15分
		// 256559秒有59秒

		// 256559秒有 2天 23時 15分 59秒

	}

}

public class Homework2 {

	public static void main(String[] args) {
		//請設計一隻Java程式，計算200顆蛋共是幾打幾顆？ (一打為12顆)
		
		int dozen = 12;//先宣告dozen值=12顆/打
		int totalEggs = 200;//再宣告totalEggs = 200顆蛋
		int resultDozen = (totalEggs / dozen);//resultDozen值=200顆蛋除以12=16打
		int sixteenDozen = (dozen * resultDozen);//sixteenDozen值:12*16=192顆蛋
		int remaining = (totalEggs - sixteenDozen);//remaining值totalEggs-sixteenDozen=8顆蛋
		System.out.println(resultDozen);//故印出有16打
		System.out.println(sixteenDozen);//故印出192顆
		System.out.println(remaining);//故印出剩餘8顆雞蛋數		
		System.out.printf("範例1:雞蛋總共%d顆 , 為%d打%d顆 \n",totalEggs,resultDozen,remaining);
		//亦可印出雞蛋總共200顆 , 為16打8顆 

	}

}
